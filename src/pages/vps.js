import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Landing from "../components/landengSection";
// import AmazingPerVPS from "../components/AmazingPerVPS";
// import ExcelentServices from "../components/ExcelentServices";
// import GoodToGo from "../components/GoodToGo";
import FeaturesVPS from "../components/FeaturesVPS";
// import InfiniteCarousel from 'react-leaf-carousel';
// import PricingVps from "../components/PricingVps"
import ImageLeft from "../components/imageLeft";
import ImageRight from "../components/imageRight";
function vpsHosting() {
    return (
        <Layout>
            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
            <Landing smallHeader=""
                     header="HOSTYLUS VPS HOSTING"
                     desc="MORE THAN WHAT YOU HAVE IN MIND, LESS THAN WHAT'S IN ACCOUNT!"
                     boldText="STARTING AT $5.99"
                     btnURL="/"
                     image="https://ik.imagekit.io/softylus/landing_vps_hosting_A-M5o7nDR.png"

            />
            <ImageRight header={"INCREDIBLE PERFORMANCE STARTING AT $5.99"}
                        desc={"Handle even your most resource-intensive projects with ease. Our cloud servers are located in  state-of-the-art data centers. You get best-in-class performance with AMD EPYC 2nd Gen, Intel® Xeon® Gold processors and speedy NVMe SSDs."}
                        image={"https://ik.imagekit.io/softylus/excelentPEr_zcUvxZZZ7.svg"}
                        hideShopNow={"hidden"}
            />

            <ImageLeft header={"GOOD TO GO IN SECONDS"}
                        desc={"Get started without waiting! With our friendly interface, you can create server instances almost instantly, usually in under 10 seconds that prevent any imposter from sneak peekingat your data"}
                        image={"https://ik.imagekit.io/softylus/goodToGo_1__dSM7nZKDo.svg"}
                        hideShopNow={"hidden"}
            />

            <ImageRight header={"EXCELLENT SERVICES"}
                        desc={"The Hostylus VPS hosting products are provided by provider whom won platinum at the Service Provider Awards 2020. Several thousand readers helped to choose the winner in a survey posted on several IT portals."}
                        image={"https://ik.imagekit.io/softylus/service_G7dzhJ8fW.svg"}
                        hideShopNow={"hidden"}
            />



     <div className="w-4/5 m-auto">
    {/*<InfiniteCarousel*/}
    {/*        pauseOnHover={true}*/}
    {/*        autoCycle={true}*/}
    {/*        breakpoints={[*/}
    {/*            {*/}
    {/*                breakpoint: 500,*/}
    {/*                settings: {*/}
    {/*                    slidesToShow: 2,*/}
    {/*                    slidesToScroll: 2,*/}
    {/*                },*/}
    {/*            },*/}
    {/*            {*/}
    {/*                breakpoint: 768,*/}
    {/*                settings: {*/}
    {/*                    slidesToShow: 4,*/}
    {/*                    slidesToScroll: 3,*/}
    {/*                },*/}
    {/*            },*/}
    {/*        ]}*/}
    {/*        dots={true}*/}
    {/*        showSides={true}*/}
    {/*        sidesOpacity={1}*/}
    {/*        sideSize={.1}*/}
    {/*        slidesToScroll={1}*/}
    {/*        slidesToShow={5}*/}
    {/*        scrollOnDevice={true}*/}

    {/*         >*/}
    {/*        <PricingVps  headerVPS={"HX11"} priceVPS={5.99} vCPU={1} RAM={2} diskSpace={20} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HPX11"} priceVPS={8.10} vCPU={2} RAM={2} diskSpace={40} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HX21"} priceVPS={10.30} vCPU={2} RAM={4} diskSpace={40} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HPX21"} priceVPS={15.99} vCPU={3} RAM={4} diskSpace={80} traffic={20}/>*/}
    {/*       <PricingVps   headerVPS={"HX31"} priceVPS={20.99} vCPU={2} RAM={8} diskSpace={80} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HPX31"} priceVPS={29.99} vCPU={4} RAM={8} diskSpace={160} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HX41"} priceVPS={36.99} vCPU={4} RAM={16} diskSpace={160} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HPX41"} priceVPS={52.99} vCPU={8} RAM={16} diskSpace={240} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HX51"} priceVPS={69.99} vCPU={8} RAM={32} diskSpace={240} traffic={20}/>*/}
    {/*        <PricingVps  headerVPS={"HPX51"} priceVPS={119.99} vCPU={16} RAM={32} diskSpace={360} traffic={20}/>*/}

    {/*    </InfiniteCarousel>*/}
         <FeaturesVPS />
</div>
            </Layout> 
       )
    }

    export default vpsHosting;

