import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Landing from "../components/landengSection";
import Feature from "../components/featureBlock";

function AboutPage() {
  return (
    <Layout>
      <SEO
        keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
        title="About"
      />
      <Landing header="ABOUT US"
               desc="Put your background behind and we will get you what you have in mind. Our speedy and secure hosting services have you covered, all for the best prices. Regardless of the type of website you’re running, we’re here to provide you with the best hosting plans you could ever think of! Our hosting platform is big enough to fit your needs"
               btnURL="/"
               image="https://ik.imagekit.io/softylus/landing_about_us_Uq7kqiHDsR.png"

      />
        <section className="features max-w-6xl mx-auto my-16 mobile:px-3">
            <h2 className="text-4xl text-black font-bold text-center">Our hosting services</h2>
            <h4 className="text-gray-400 text-center text-xl px-16 my-3">More than what you have in mind, less than what’s in account!</h4>
            <div className="features grid grid-cols-3 gap-3 flex justify-items-stretch  mt-6 mobile:grid-cols-1 py-8">
                <Feature image={"https://ik.imagekit.io/softylus/wordpress_hosting_gilvjAvjGC.svg"}
                         header={"WordPress Hosting"}
                />
                <Feature image={"https://ik.imagekit.io/softylus/business_hosting_5hMUooLfR.svg"}
                         header={"Business Hosting"}
                />
                <Feature image={"https://ik.imagekit.io/softylus/shared_hosting_lUtkIdadvU.svg"}
                         header={"Shared Hosting"}
                />
                <Feature image={"https://ik.imagekit.io/softylus/Cloud_VPN_Na2EUjSnZ.svg"}
                         header={"VPS Hosting"}
                />
                <Feature image={"https://ik.imagekit.io/softylus/deticated_servres_81yQg0W-W.svg"}
                         header={"Dedicated Servers"}
                />
                <Feature image={"https://ik.imagekit.io/softylus/domains_sItFpk1C9y.svg"}
                         header={"Domains"}
                />

            </div>
        </section>
        <section className="h-72 bg-blue-600 flex flex-wrap content-center justify-center">
            <div className="max-w-3xl text-center">
            <p className="text-white text-md my-6">Hostylus offers you the best packages to start for the best prices, and without compromising on the quality. Despite that we&apos;d like to keep you at ease and assure you a 30 day money return if you ever decide to cancel.</p>
            <button className="px-6 inline py-3 uppercase bg-white shadow-md ml-1 text-blue-600 rounded-3xl font-medium text-sm">get started</button>
        </div>
        </section>
    </Layout>
  );
}

export default AboutPage;
