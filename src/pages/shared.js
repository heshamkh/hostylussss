import React from "react";
import SEO from "../components/seo";
import Layout from "../components/layout";
import Accordion from "../components/accordion";
import ImageRight from "../components/imageRight";
import ImageLeft from "../components/imageLeft";
import Landing from "../components/landengSection";
import LI from "../components/plan-li";
import PlanCard from "../components/planSelectCard";
import Tabs from "../components/tabs";
function business() {
    return(
        <Layout>

            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
            <Landing header="Affordable Shared Hosting"
                     desc="A hosting service is a necessity if you’re willing to take over with your online business. And for you, we pull the ropes together to provide you with quality and price that fit within your budget, so quit stalling and get it over with us!"
                     btnURL="/"
                     image="https://ik.imagekit.io/softylus/landing_shared_hosting_GrPe8sU4V.png"

            />
            <section className="sharedPlans max-w-5xl mx-auto my-16 mobile:pb-3">
                <h2 className="text-3xl text-black font-bold text-center">Upgraded plans today for a better tomorrow</h2>
                <h6 className="text-gray-400 text-center">Unleash the technology of your mind, and elevate your business sales with our business hosting plans</h6>
                <div className="g grid grid-cols-3 gap-3 flex justify-items-center mt-8 mobile:grid-cols-1 tablet:grid-cols-1 py-8">
                    <PlanCard price1m={11.99} price12m={119.88} price24m={216.00} price36m={288.00}
                              header={"startup"}  btnLink={"/"}
                              websites={"1 website"} RAM={"1000MB"} freeDomain={"no"} subDomains={"1"} parkedDomains={"1"} emailAccounts={"10"} DBs={"6"} FTP={"3"}
                    />
                    <PlanCard price1m={16.99} price12m={179.88} price24m={335.76} price36m={467.64}
                              header={"level up"}  btnLink={"/"}
                              websites={"3 websites"} RAM={"2000MB"} freeDomain={"Yes(.COM)"} subDomains={"4"} parkedDomains={"3"} emailAccounts={"20"} DBs={"3"} FTP={"1"}
                    />
                    <PlanCard price1m={11.99} price12m={119.88} price24m={216.00} price36m={288.00}
                              header={"bossed up"}  btnLink={"/"}
                              websites={"unlimited"} RAM={"3000MB"} freeDomain={"Yes(.COM)"} subDomains={"unlimited"} parkedDomains={"unlimited"} emailAccounts={"unlimited"} DBs={"unlimited"} FTP={"unlimited"}
                    />
                </div>
            </section>

            <ImageRight image={"https://ik.imagekit.io/softylus/migrationHub_YcJv4LtkL.png"}
                        header={"Migration Hub"}
                        smallHeader={"Migration has never been easier"}
                        desc={"Fear no loss in data or ranking while migrating your website with recovery plans that prevent any imposter from sneak peeking at your data"} hideShopNow={"hidden"}/>
            <ImageLeft image={"https://ik.imagekit.io/softylus/server_5gzudTSf0.png"}
                       header={"Steadfast Linux Servers"}
                       desc={<ul style={{listStyleImage: "url('https://ik.imagekit.io/softylus/blueTick_nzCxIZ4xA.svg')",listStylePosition: "inside",textAlign:"left"}} className="list-disc">
                           <LI LI={"24/7 support from our hosting experts."} textSize={"text-lg"}/>
                           <LI LI={"Application installers including WordPress Hosting"} textSize={"text-lg"}/>
                           <LI LI={"FREE migration to our servers"} textSize={"text-lg"}/>
                           <LI LI={"Latest PHP 7.0 - 7.4"} textSize={"text-lg"}/>
                           <LI LI={"Daily backups"} textSize={"text-lg"}/>
                           <LI LI={"Firewall + Security package"} textSize={"text-lg"}/>
                       </ul>}
                       hideShopNow={"hidden"} hideLearnMore={"hidden"}/>
            <section className="bg-gray-100 py-16">
                <div className="px-32 mobile:px-3 tablet:px-8">
                    <h3 className="text-center text-black mb-4 text-4xl font-bold">Plesk</h3>
                    <h4 className="text-gray-400 text-center mb-8 text-md px-16 my-3">Revealing Plesk next-level server management platform</h4>
                    <Tabs/>
                </div>
            </section>
            <section className="acc py-16 grid justify-items-center">
                <div className=" px-8 max-w-2xl">
                    <h3 className="text-center text-black mb-16 text-4xl font-bold uppercase">Entery-level Let’s encrypt protection</h3>
                    <Accordion title={"HSTS"} content={"Enhances the security of website’s visitors by prohibiting web browsers from accessing the website via insecure HTTP connections. If visitors are unable to connect via HTTPS, your website will become unavailable."}/>
                    <Accordion title={"OCSP Stapling"} content={"Enhances the privacy of website’s visitors and improves the website performance. The web server will request the status of the website’s certificate (can be good, revoked, or unknown) from the CA instead of the visitor’s browser doing so."}/>
                    <Accordion title={"SSL Labs Test"} content={"Deep analysis of the SSL web server configuration."}/>
                    <Accordion title={"Keep websites secured"} content={"Automatically replaces expired or self-signed SSL/TLS certificates with free valid certificates from Let’s Encrypt. Covers each domain, subdomain, domain alias, and webmail belonging to the subscription."}/>
                </div>
            </section>

        </Layout>
    );

}
export default business;