import React from "react";
import SEO from "../components/seo";
import Layout from "../components/layout";
import Feature from "../components/featureBlock";
import Accordion from "../components/accordion";
import ImageRight from "../components/imageRight"
function domains() {
    return(
        <Layout>
            <SEO
                keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
                title="Home"
            />
        <section className="py-32" style={{backgroundImage:'url("https://ik.imagekit.io/softylus/logo_with_bg_NuhXLDww4.jpg")',backgroundPosition:'center',backgroundSize:'cover'}}>
                <div className="max-w-4xl mx-auto p-8 mobile:pb-3">
                    <h2 className="text-2xl text-center font-bold text-black">Looking for your identity to be named?</h2>
                    <h6 className="text-md mb-8 mt-2 text-center text-black">We&apos;ll generate it for you!</h6>
                    <div className="mobile:my-4 my-8 bg-white grid grid-cols-5 mobile:grid-cols-4 p-1 mobile:p-0.5 border-2 border-gray-200 rounded-full">
                        <input className="mr-2 bg-none col-span-4 mobile:col-span-3 mobile:py-2 px-4 py-3  rounded-full" placeholder="Domain.com"/>
                        <button className="flex justify-center text-white font-bold  bg-blue-600 rounded-full mobile:text-sm mobile:px-8  py-3">
                            <svg className="mr-2 mobile:hidden" width="23" height="24" viewBox="0 0 23 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.1516 0.685449C4.72926 0.685449 0.317773 5.09584 0.317773 10.5169C0.317773 15.9384 4.72926 20.3484 10.1516 20.3484C15.5742 20.3484 19.9854 15.9384 19.9854 10.5169C19.9854 5.09584 15.5742 0.685449 10.1516 0.685449ZM10.1516 18.2611C5.88055 18.2611 2.40555 14.7869 2.40555 10.517C2.40555 6.24707 5.88055 2.7728 10.1516 2.7728C14.4226 2.7728 17.8976 6.24703 17.8976 10.5169C17.8976 14.7869 14.4226 18.2611 10.1516 18.2611Z" fill="white" stroke="white" strokeWidth="0.3"/>
                                <path d="M17.1759 16.0645L22.3008 21.1882C22.7087 21.5956 22.7087 22.2567 22.3008 22.6641C22.097 22.8678 21.8295 22.9698 21.5628 22.9698C21.2958 22.9698 21.0286 22.8678 20.8248 22.6641L15.6998 17.5403C15.6998 17.5403 15.6998 17.5403 15.6998 17.5403C15.292 17.1329 15.292 16.4719 15.6998 16.0645L17.1759 16.0645ZM17.1759 16.0645C16.7681 15.6568 16.1076 15.6568 15.6998 16.0644L17.1759 16.0645Z" fill="white" stroke="white" strokeWidth="0.3"/>
                            </svg>
                            Search
                        </button>
                    </div>

                    <div className=" flex justify-center">
                        <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-black font-bold ">.Com</span>
                        <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-black font-bold">.Co</span>
                        <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-black font-bold">.Net</span>
                        <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-black font-bold">.Org</span>
                        <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-black font-bold">.Club</span>
                        <span className="mobile:p-0.5 p-2 mx-0.5 text-sm text-black font-bold">.Design</span>
                        <span className="mobile:p-0.5 p-2  mx-0.5 text-sm text-black font-bold">.Xyz </span>
                    </div>
                </div>
            </section>
            <ImageRight image={"https://ik.imagekit.io/softylus/ssl_4R8G7Vw-Nlewi.png"} header={"Website Security Shop SSL Certificates"} desc={"Protect your website and your visitors from hacks and data breaches with a secure connection"} hideLearnMore={"hidden"}/>
            <section className="features max-w-6xl mx-auto my-16 mobile:px-3 tablet:px-6 laptop:px-8">
                <h2 className="text-4xl text-black font-bold text-center">Domain Name Features</h2>
                <h4 className="text-gray-400 text-center text-xl px-16 my-3">A domain name tells the world who you are and what you do. Search for a name, word, or phrase and we’ll help you find the perfect domain name to own your piece of the internet.</h4>
                <div className="features grid grid-cols-3 gap-3 flex justify-items-center mt-6 mobile:grid-cols-1 py-8">
                    <Feature image={"https://ik.imagekit.io/softylus/Dns_W_PIp718u.png"}
                             header={"DNS Management"}
                             desc={"Once you use our domain name lookup and find a suitable web address, you can manage your site through a simple interface"}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/renewal_4usKL-69uosb.png"}
                             header={"Auto-Renewal"}
                             desc={"Protect your domain from expiring and accidentally being lost by enabling auto-renewal. Switch back to manual renewal at anytime. "}
                    />
                    <Feature image={"https://ik.imagekit.io/softylus/Dlock_dn70RqayNC.png"}
                             header={"Domain Lock"}
                             desc={"Once you find your perfect domain, lock it down to prevent unauthorized transfers. Unlock it for transfers at anytime. "}
                    />

                </div>
            </section>
            <section className="acc py-16 grid justify-items-center" >
                <div className=" px-8 max-w-2xl">
                    <h3 className="text-center text-black mb-16 text-4xl font-bold uppercase">Frequently Asked Questions</h3>
                    <Accordion title={"What is a domain name?"} content={"A domain name is your website name, the address where Internet users can access your website. used for finding and identifying computers on the Internet."}/>
                    <Accordion title={"Is My Domain Name Available?"} content={"if A domain name is already claimed by someone else, it can’t be registered even if it’s not actually being used, but it’s easy to find out if your domain name is available in the form you want. the most reliable way to check for the availability of a domain name is to use a domain checker that can scan all unavailable names."}/>
                    <Accordion title={"How Should I Choose My Domain Name?"} content={"Choose a domain name that is easy to type, Avoid numbers and hyphens and keep it short. Try using keywords that describe your business and the services you offer. make sure to choose Extension that works for your business. NOTE :Extensions are suffixes, such as .com or .net, at the end of web addresses."}/>
                    <Accordion title={"Why Do I Need a Domain Name?"} content={"A domain name is a valuable marketing and search tool that should successfully lead customers to your site. Careful deliberation and research should be applied, as it might be the most important decision you make when carving out your slice of the online market."}/>
                    <Accordion title={"How Do Domains Work?"}
                               content={<ol>
                                   <li>When you enter a domain name in your web browser, it first sends a request to a global network of servers that form the Domain Name System (DNS).</li>
                                   <li>These servers then look up for the name servers associated with the domain and forward the request to those name servers.</li>
                                   <li>These name servers are computers managed by your hosting company. Your hosting company will forward your request to the computer where your website is stored.</li>
                                   <li>This computer is called a web server. It has special software installed (Apache, Nginx are two popular web server software). The web server now fetches the web page and pieces of information associated with it.</li>
                                   <li>Finally, it then sends this data back to the browser.</li></ol>}/>
                </div>
            </section>

        </Layout>
    );

}
export default domains;