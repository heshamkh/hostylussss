import React from "react";
import PropTypes from "prop-types";
function wordpressHosting(props) {
    return (

            <div className="flex  bg-gray-50  mobile:flex-col-reverse">
                <div className="w-1/2  my-auto mobile:my-4 mobile:text-center tablet:align-bottom tablet:m-auto mobile:w-full tablet:w-full ">
                    <span className="text-sm block w-2/3 m-auto mobile:text-xs uppercase font-bold tracking-widest text-black opacity-50 mobile:text-center tablet:text-center">{props.smallHeader}</span>
                    <h1 className="capitalize font-bold w-2/3 m-auto mobile:mt-5  text-4xl mobile:text-2xl tablet:text-3xl tablet:mt-5 mb-3 ">{props.header}</h1>

                    <p className="font-semibold text-gray-400  w-2/3 my-3 m-auto tablet:text-sm mobile:w-full mobile:px-10   mobile:text-center">{props.desc} <span className="font-black">{props.boldText}</span></p>
                    <div className="w-2/3 mx-auto ">
                    <a href={props.btnURL}>
                        <button className=" bg-blue-600 py-2 tablet:text-xs font-bold text-white mobile:mr-3  rounded-3xl px-8 border-2 mb-2 mt-8 tablet:mt-4 mobile:mt-4 uppercase">get started</button>
                    </a></div>
                </div>

                <div className="laptop:w-1/2 mx-10 mobile:m-auto mobile:text-center tablet:text-center mobile:w-full tablet:w-full" >
                    <img className="m-auto mobile:p-4" src={props.image}/>
                </div>
            </div>
    )
}
export default wordpressHosting;
wordpressHosting.PropTypes={
    image: PropTypes.string.isRequired, // must be a string and defined
    smallHeader: PropTypes.string.isRequired, // must be a string and defined
    header: PropTypes.string.isRequired, // must be a string and defined
    desc: PropTypes.string.isRequired, // must be a string and defined
    btnURL:PropTypes.string.isRequired,
    boldText:PropTypes.string,
}