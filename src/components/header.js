import React, { useState }  from 'react';
import {Link} from "gatsby";

function Header() {
    // let dropdown=false;
    const [dropdown, setCheck] = useState(false);
function openMenu(){
    setCheck(prevDropdown => !prevDropdown);
}


    return(
  // <div className="w-full h-20 mb-10 align-middle bg-white shadow">
    
  //   <div className=" sm:w-full w-1/5 h-full align-middle inline-block">
  //     <img className="m-auto h-full w-1/2" src="https://ik.imagekit.io/softylus/logo_hostylus_91QTyXifp.svg"></img>
  //   </div>

  //   <div className="w-4/5 h-full align-middle inline-block text-right">
  //     <ul className="inline-block ">
  //       <li className="inline-block text-sm  hover:font-bold delay-50  border-b-4 border-transparent mx-2 px-3  py-7 hover:border-blue-800 "><a className="py-7 " href="#">Home</a></li>
  //       <li className="inline text-sm  hover:font-bold delay-50   border-b-4 border-transparent mx-3 px-2  py-7 hover:border-blue-800 "><a className=" py-7" href="#">Web Hosting</a></li>
  //       <li className="inline text-sm  hover:font-bold delay-50  border-b-4 border-transparent mx-3 px-2  py-7  hover:border-blue-800 "><a className=" py-7" href="#">Servers</a></li>
  //       <li className="inline text-sm  hover:font-bold delay-50   border-b-4 border-transparent mx-3 px-2 py-7 hover:border-blue-800 "><a  className="hover:font-bold py-7" href="#">Domains</a></li>
  //       <li className="inline text-sm hover:font-bold delay-50  border-b-4 border-transparent mx-3 px-2 py-7 hover:border-blue-800  "><a className=" py-7" href="#">More</a></li>
  //     </ul>

  //     <button className="px-6 inline py-3 mr-16 uppercase bg-blue-600 shadow-md ml-1 text-white rounded-3xl font-medium text-sm "> client area</button>
  //   </div>
  // </div>
 <header>
     <div className="mobile:hidden tablet:hidden lg:px-20 px-6  bg-white shadow flex flex-wrap  items-center lg:py-0 mobile:py-0 tablet:py-0 py-3">
     <div className=" flex-1 flex justify-between items-center ">
         <Link to="/">
             <img className="my-auto ml-20 pl-3 w-1/3 mobile:w-12 mobile:my-2 tablet:ml-11 tablet:pl-0 mobile:ml-2" src="https://ik.imagekit.io/softylus/hostylus_RylJbcnlB.png"/>

         </Link>
     </div>

    <div className="visible mobile:hidden desktop:w-4/5 tablet:w-1/2 mobile:w-1/2 h-full align-middle inline-block text-right">
       <ul className="inline-block mobile:invisible tablet:invisible ">
           <li className="inline-block text-sm  hover:font-bold delay-50  border-b-2 border-transparent mx-2 px-3  py-3 hover:border-blue-600 "> <Link className=" hover:font-bold py-7" to="/">Home</Link></li>
         <li className="inline text-sm  hover:font-bold delay-50   border-b-2 border-transparent mx-3 px-2  py-3 hover:border-blue-600 ">
             <div className="relative inline-block text-left">
                     <button type="button"
                             className=" inline-flex justify-center focus:outline-none transition duration-150 ease-in-out"
                             id="options-menu" aria-haspopup="true" aria-expanded="true"
                 onClick={openMenu}>
                         Web Hosting
                         {/* Heroicon name: solid/chevron-down */}
                         <svg className="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                              fill="currentColor" aria-hidden="true">
                             <path fillRule="evenodd"
                                   d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                   clipRule="evenodd"/>
                         </svg>
                     </button>
                 {/*//     Dropdown panel, show/hide based on dropdown state.*/}
                 {/*//     Entering: "transition ease-out duration-100"*/}
                 {/*//       From: "transform opacity-0 scale-95"*/}
                 {/*//       To: "transform opacity-100 scale-100"*/}
                 {/*//     Leaving: "transition ease-in duration-75"*/}
                 {/*//       From: "transform opacity-100 scale-100"*/}
                 {/*//       To: "transform opacity-0 scale-95"*/}
             <div
                 className={`bg-white origin-top-right absolute -right-1/2 mt-2 w-48 rounded-md shadow-lg transition ease-${dropdown ? 'out' : 'in'} duration-${dropdown ? '75' : '75'} transform opacity-${dropdown ? '100' : '0'} scale-${dropdown ? '100' : '95'}`}>
                 <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                     <Link to={"/wordpress"} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                           role="menuitem">WordPress hosting</Link>
                     <Link to={"/shared"} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                           role="menuitem">Shared Hosting</Link>
                     <Link to={"/Business"} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                           role="menuitem">Business Hosting</Link>
                 </div>
             </div>
             </div>
         </li>
           <li className="inline text-sm  hover:font-bold delay-50  border-b-2 border-transparent mx-3 px-2  py-3  hover:border-blue-600 "><Link to={"/vps"} className=" hover:font-bold py-7" >Servers</Link></li>
           <li className="inline text-sm  hover:font-bold delay-50   border-b-2 border-transparent mx-3 px-2 py-3 hover:border-blue-600 "><Link to={"/domains"} className="hover:font-bold py-7" >Domains</Link></li>
           <li className="inline text-sm  hover:font-bold delay-50   border-b-2 border-transparent mx-3 px-2 py-3 hover:border-blue-600 "><Link to={"/about"} className="hover:font-bold py-7" >About us</Link></li>
           <li className="inline text-sm hover:font-bold delay-50  border-b-2 border-transparent mx-3 px-2 py-3 hover:border-blue-600  "><a className=" py-7" href="#">More</a></li>
       </ul>

       <button className=" mobile:invisible tablet:invisible  px-6 inline py-3 mr-16 uppercase bg-blue-600 shadow-md ml-1 text-white rounded-3xl font-medium text-sm "> client area</button>
     </div>
     </div>
     {/*end desktop menu */}
<div className="my-1">

<div className="grid gap-4 grid-cols-3 laptop:hidden desktop:hidden wide-desktop:hidden">
    <div>

    </div>
    <div className="flex-1 flex justify-between items-center text-center">
        <Link to="/" className="m-auto">
            <img className=" pl-3 w-16 my-4 mobile:w-12 mobile:my-2 " src="https://ik.imagekit.io/softylus/hostylus_RylJbcnlB.png"/>

        </Link>
    </div>
    <label htmlFor="menu-toggle" className="flex-1 flex items-center pointer-cursor laptop:hidden desktop:hidden wide-desktop:hidden block "><svg className="fill-current text-gray-900 m-auto" xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 20 20"><title>menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg></label>

</div>
  <input className="hidden" type="checkbox" id="menu-toggle" />
<div className="hidden lg:flex lg:items-center mobile:w-full tablet:w-full" id="menu">
    <nav className="mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
      <ul className="sm:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
        <li className="lg:p-4 py-3 px-0 block"><Link className=" border-b-2 border-transparent hover:border-blue-600" to="/">Home</Link></li>
        <li className="lg:p-4 py-3 px-0 block">
            <div className="">
                <button type="button"
                        className=" inline-flex justify-center focus:outline-none transition duration-150 ease-in-out border-b-2 border-transparent hover:border-blue-600"
                        id="options-menu" aria-haspopup="true" aria-expanded="true"
                        onClick={openMenu}>
                    Web Hosting
                    {/* Heroicon name: solid/chevron-down */}
                    <svg className="mt-1 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                         fill="currentColor" aria-hidden="true">
                        <path fillRule="evenodd"
                              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                              clipRule="evenodd"/>
                    </svg>
                </button>
                {/*//     Dropdown panel, show/hide based on dropdown state.*/}
                {/*//     Entering: "transition ease-out duration-100"*/}
                {/*//       From: "transform opacity-0 scale-95"*/}
                {/*//       To: "transform opacity-100 scale-100"*/}
                {/*//     Leaving: "transition ease-in duration-75"*/}
                {/*//       From: "transform opacity-100 scale-100"*/}
                {/*//       To: "transform opacity-0 scale-95"*/}
                <div
                    className={`mt-2 transition ease-${dropdown ? 'out' : 'in'} duration-${dropdown ? '100' : '75'} transform ${dropdown ? 'block' : 'hidden'} scale-${dropdown ? '100' : '95'}`}>
                    <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                        <Link to={"/wordpress"} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                              role="menuitem">WordPress hosting</Link>
                        <Link to={"/shared"} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                              role="menuitem">Shared Hosting</Link>
                        <Link to={"/Business"} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                              role="menuitem">Business Hosting</Link>
                    </div>
                </div>
            </div>
        </li>
        <li className="lg:p-4 py-3 px-0 block"><Link className="border-b-2 border-transparent hover:border-blue-600" to="/domains">Domain</Link></li>
        <li className="lg:p-4 py-3 px-0 block"><Link className="border-b-2 border-transparent hover:border-blue-600" to="/vps">Servers</Link></li>
          <li className="lg:p-4 py-3 px-0 block"><Link className="border-b-2 border-transparent hover:border-blue-600" to="/about">About Us</Link></li>
          <li className="lg:p-4 py-3 px-0 block"><a className="border-b-2 border-transparent hover:border-blue-600" href="#">More</a></li>
      </ul>
       <button className="px-6 inline py-3 mr-16 uppercase bg-blue-600 shadow-md mb-6 ml-1 text-white rounded-3xl font-medium text-sm tablet:mr-0 mobile:mr-0"> client area</button>

    </nav>
   </div>
</div>
  </header>
  );
}

export default Header;





