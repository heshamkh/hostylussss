import React from "react";


function AmazingPerVPS() {
    return (
          <div className=" flex m-auto laptop:my-10 bg-white mobile:my-10 mobile:flex-col-reverse ">
    
    <div className="w-1/2  my-auto mobile:my-4 mobile:text-center tablet:align-bottom tablet:my-0 mobile:w-full tablet:w-full ">
           
        <h1 className="  font-extrabold w-2/3 m-auto mobile:mt-5  text-3xl mb-3 ">INCREDIBLE PERFORMANCE STARTING AT $5.99</h1>
            <p className="font-light  w-2/3 my-3 m-auto mobile:w-full mobile:px-10   mobile:text-center">
              Handle even your most resource-intensive projects with ease.
Our cloud servers are located in  state-of-the-art data centers. You get best-in-class performance with AMD EPYC 2nd Gen, Intel® Xeon® Gold processors and speedy NVMe SSDs.
            </p>
               <div className="w-2/3 mx-auto flex flex-row">
                   <span className="text-blue-600 font-black uppercase text-md underline mr-2">Learn more</span>
                   <img className="inline-block" src="https://ik.imagekit.io/softylus/arrow_b_EEPW_gX.svg"></img>
               </div>
         
         </div>

        <div className="laptop:w-1/2 mx-10 my-10 mobile:m-auto mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
            <img className="m-auto mobile:p-4" src="https://ik.imagekit.io/softylus/excelentPEr_zcUvxZZZ7.svg"></img>
      </div>
  </div>

            

        
       )
    }

    export default AmazingPerVPS;

