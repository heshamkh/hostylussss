import React from "react";
import { Link } from "gatsby";
import PropTypes from "prop-types";
function nav_li(props) {
return(
    <li className="font-lexend-deca text-black opacity-70 font-400 text-sm py-2 mobile:text-center">
        <Link to={props.LinkSlug}>{props.LinkText}</Link>
    </li>
)
}
nav_li.propTypes = {
    LinkSlug: PropTypes.string.isRequired, // must be a string and defined
    LinkText: PropTypes.string.isRequired, // must be a number and defined
};
export default nav_li