import React from "react";
import PropTypes from "prop-types";


export default function vpsPricing(props){
 return(
     <div className="p-4 border-solid border-gray-200 border-2 rounded-lg my-16 hover:border-blue-600 w-72 mobile:w-full">
       
         
         <h4 className="text-black text-3xl mobile:text-2xl text-center font-extrabold my-6">{props.headerVPS}</h4>
         <div className="w-full h-24 flex flex-wrap content-center justify-center text-center bg-gray-50">
             <div>
                <sup className="text-black font-semibold text-2xl mobile:text-xl">$</sup>  <span className="m-auto text-blue-600 font-black text-4xl mobile:text-3xl">{props.priceVPS}</span><span className="text-gray-400 font-semibold text-xl mobile:text-lg">/Month</span>
            </div>
         </div>
         <div className="px-3">
             <ul className="font-bold list-disc" style={{listStyleImage: "url('https://ik.imagekit.io/softylus/tick_PLS8tkQNa.jpg')",listStylePosition: "inside",listStyleSize:"2px"}}>
                 <li className="my-3 mobile:my-2">{props.vCPU} vCPU</li>
                 <li className="my-3 mobile:my-2">{props.RAM}GB RAM</li>
                 <li className="my-3 mobile:my-2">{props.diskSpace}GB Disk Space</li>
                 <li className="my-3 mobile:my-2">{props.traffic}GB Traffic</li>
             </ul>

         </div>
     </div>
 );
}
vpsPricing.PropTypes={
    priceVPS: PropTypes.number.isRequired, // must be a number and defined
    vCPU: PropTypes.number.isRequired, // must be a number and defined
    RAM: PropTypes.number.isRequired, // must be a number and defined
    diskSpace: PropTypes.number.isRequired, // must be a number and defined
    traffic: PropTypes.number.isRequired, // must be a number and defined
    headerVPS:PropTypes.string.isRequired,
}