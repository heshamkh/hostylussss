import React from "react";
import PropTypes from "prop-types";
function imageLeft(props) {
    return(
<section className="px-32 mobile:px-2 tablet:px-4">
        <div className=" flex laptop:my-10 bg-white mobile:flex-col ">

            <div className="w-1/2 laptop:w-2/3 ml-5 mt-10 mobile:text-center tablet:text-center mobile:w-full tablet:w-full">
                <img className="mobile:mt-8  mobile:p-4 m-auto" src={props.image}></img>
            </div>

            <div className="ml-8 w-1/2 mobile:ml-0 tablet:ml-0 m-auto mobile:text-center tablet:px-4 mobile:m-4 mobile:w-full tablet:w-full ">

                <h1 className=" tablet:text-xl font-extrabold uppercase text-3xl mb-3 ">{props.header}</h1>
                <div className="font-light my-3 mobile:w-full mobile:pl-4 mobile:pr-8  tablet:w-full mobile:text-center">
                    {props.desc}</div>
                <div className={` mx-auto flex flex-row ${props.hideLearnMore}`}>
                    <a href={props.learnMore} className="inline-block text-blue-600 font-black uppercase text-md underline mr-2">learn more</a>
                    <img className="inline-block" src="https://ik.imagekit.io/softylus/arrow_b_EEPW_gX.svg"/>
                </div>
                <div className={` mx-auto my-6 flex flex-row ${props.hideShopNow}`}>
                    <a href={"/"} className="text-black-600 font-semibold uppercase text-md rounded-full border-2 border-black py-2 px-6 mr-2">shop now</a>
                </div>
            </div>

        </div>
</section>

    );
}

export default imageLeft;
imageLeft.propTypes = {
    image:PropTypes.string.isRequired, // must be a string and defined
    header:PropTypes.string.isRequired, // must be a string and defined
    desc:PropTypes.any.isRequired, // must be a string and defined
    learnMore:PropTypes.string, // must be a string and defined
    hideLearnMore:PropTypes.string, // must be a string and defined
    hideShopNow:PropTypes.string, // must be a string and defined
};